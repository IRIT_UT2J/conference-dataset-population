TODO:
- script that goes through all the queries, gets a list of all the source entities
- compare the list of all source entities with the list of all populated classes in each ontology (what classes are not covered by the queries)

# notes
- precision oriented
- a distinction was made between "X reviews paper Y" meaning X has been assigned Y and "X reviews paper Y": X actually writes the review for Y.

- problems spotted: 
 * missing startdate in edas (for social events)
 * in ekaw: missing presenter, missing presentation part of conf, missing link between invited speaker and invited abstract







